package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {
private EditText et_email,et_password;
SharedPref sharedPref;
CheckBox checkBoxRemember;

    FirebaseAuth authRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_email=(EditText)findViewById(R.id.email);
        et_password=(EditText)findViewById(R.id.password);
        sharedPref = new SharedPref(this);
        checkBoxRemember = findViewById(R.id.checkBoxRemember);

        authRef = FirebaseAuth.getInstance();
    }

    public void openLogin(View view) {
        final String email,password;
        email = et_email.getText().toString();
        password= et_password.getText().toString();


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.show();


        if (TextUtils.isEmpty(email)) {
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Please enter email...", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Please enter password!", Toast.LENGTH_LONG).show();
            return;
        }


        authRef.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d("Tags","berhasil login");
                    if (checkBoxRemember.isChecked()) {
                        sharedPref.setString(SharedPref.EMAIL, email);
                        sharedPref.setString(SharedPref.PASSWORD, password);
                        sharedPref.setBoolean(SharedPref.formatKey(SharedPref.REMEMBER, authRef.getCurrentUser().getUid()), true);
                    }
                        if(email.equals("admin@ibeca.com")){
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Berhasil Login!", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), MainAdminActivity.class);
                            startActivity(intent);
                        }else{
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Berhasil Login!", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                }else{
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Gagal Login!", Toast.LENGTH_LONG).show();
                    Log.d("tags","error login"+task.getException());

                }
            }
        });

//        if (et_email.getText().toString() .equals( "admin@ibeca.com") && et_password.getText().toString() .equals("123") ){
//            Intent intent = new Intent(this, MainAdminActivity.class);
//            startActivity(intent);
//        }
//        else {
//            Intent intent = new Intent(this, AppIntroActivity.class);
//            startActivity(intent);
//        }

    }

    public void forgotPassword(View view) {
        Intent intent = new Intent(this, ForgetPasswordVerificationActivity.class);
        startActivity(intent);

    }
}
