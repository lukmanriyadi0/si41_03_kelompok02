package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class AdminLihatMotiv extends AppCompatActivity {
    private Uri filePath;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    private PDFView pdfView;

    ///dari intent
    String id_user;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_lihat_motiv);

        Intent intent = getIntent();
        id_user = intent.getStringExtra("id_mahasiswa");

        //getting views from layout
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Motivation Letter");
        toolbar.setTitleTextAppearance(getApplicationContext(), R.style.ToolbarTitleTextStyle);

        pdfView =findViewById(R.id.pdfView);
        String path = "Mahasiswa/"+id_user+"/filemotiv.pdf";
        final StorageReference fileReference = storageReference.child(path);
        fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'


                fileReference.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        // Use the bytes to display the image
                        pdfView.fromBytes(bytes).load();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle any errors
                        Log.d("TAG",exception.getMessage());
                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags","file not found");
            }
        });
    }

}
