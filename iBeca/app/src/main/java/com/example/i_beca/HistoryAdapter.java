package com.example.i_beca;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {
    private ArrayList<HistoryModel> mHistoryArray;
    public HistoryAdapter(ArrayList<HistoryModel> historyarray){
        mHistoryArray = historyarray;
    }

    public static class HistoryViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextDate;
        public TextView mTextName;
        public TextView mTextParticipant;
        public TextView mTextTahap;
        public TextView mTextStatus;

        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageHistory);
            mTextDate = itemView.findViewById(R.id.textDate);
            mTextName = itemView.findViewById(R.id.textBeasiswa);
//            mTextParticipant = itemView.findViewById(R.id.textParticipant);
            mTextTahap = itemView.findViewById(R.id.textTahap);
            mTextStatus = itemView.findViewById(R.id.textStatus);
        }
    }


    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_history, parent, false);
        HistoryViewHolder hvh = new HistoryViewHolder(v);
        return hvh;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        HistoryModel currentItem = mHistoryArray.get(position);
        holder.mImageView.setImageResource(currentItem.getImageResource());
        holder.mTextDate.setText(currentItem.getTextDate());
        holder.mTextName.setText(currentItem.getTextName());
        holder.mTextParticipant.setText(currentItem.getTextParticipant());
        holder.mTextTahap.setText(currentItem.getTextTahap());
        holder.mTextStatus.setText(currentItem.getTextStatus());
    }

    @Override
    public int getItemCount() {
        return mHistoryArray.size();
    }
}
