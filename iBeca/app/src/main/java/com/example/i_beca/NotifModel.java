package com.example.i_beca;

public class NotifModel {
    private String id, id_user;
    private String title,subtitle;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public NotifModel(String id, String title, String subtitle) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
    }
    public NotifModel(String id, String id_user, String title, String subtitle) {
        this.id = id;
        this.id_user = id_user;
        this.title = title;
        this.subtitle = subtitle;
    }
}
