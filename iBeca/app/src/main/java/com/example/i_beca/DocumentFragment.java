package com.example.i_beca;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.sql.Ref;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DocumentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DocumentFragment extends Fragment implements View.OnClickListener {
    LinearLayout clickableFoto;
    LinearLayout clickableTranskrip;
    LinearLayout clickableMotiv;
    LinearLayout clickableGaji;
    LinearLayout clickableCv;
    LinearLayout clickableSkor;

    TextView adaFoto,adaTranskrip,adaCv,adaSkor,adaMotiv,adaGaji;

    private FirebaseUser authRef = FirebaseAuth.getInstance().getCurrentUser();
    private StorageReference storageRef = FirebaseStorage.getInstance().getReference();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    StorageReference pasFotoRef = storageRef.child("Mahasiswa/" + authRef.getUid() + "/Filefoto.jpg");
    Boolean pasFoto = false;
    StorageReference transkripNilaiRef = storageRef.child("Mahasiswa/" + authRef.getUid() +"/filetranskrip.pdf");
    Boolean transkrip = false;
    StorageReference cvRef = storageRef.child("Mahasiswa/" + authRef.getUid() + "/filecv.pdf");
    Boolean cv = false;
    StorageReference skorRef = storageRef.child("Mahasiswa/" + authRef.getUid() + "/fileskor.jpg");
    Boolean skor = false;
    StorageReference motivRef = storageRef.child("Mahasiswa/" + authRef.getUid() + "/filemotiv.pdf");
    Boolean motiv = false;
    StorageReference gajiRef = storageRef.child("Mahasiswa/" + authRef.getUid() + "/filegaji.jpg");
    Boolean gaji = false;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DocumentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DocumentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DocumentFragment newInstance(String param1, String param2) {
        DocumentFragment fragment = new DocumentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        clickableFoto = view.findViewById(R.id.btnfoto);
        clickableFoto.setOnClickListener(this);

        clickableTranskrip = view.findViewById(R.id.btntranskrip);
        clickableTranskrip.setOnClickListener(this);

        clickableMotiv = view.findViewById(R.id.btnmotiv);
        clickableMotiv.setOnClickListener(this);

        clickableGaji = view.findViewById(R.id.btngaji);
        clickableGaji.setOnClickListener(this);

        clickableCv = view.findViewById(R.id.btncv);
        clickableCv.setOnClickListener(this);

        clickableSkor = view.findViewById(R.id.btnskor);
        clickableSkor.setOnClickListener(this);

        adaFoto = view.findViewById(R.id.adafoto);
        adaTranskrip = view.findViewById(R.id.adatranskrip);
        adaCv = view.findViewById(R.id.adacv);
        adaSkor = view.findViewById(R.id.adaskor);
        adaMotiv = view.findViewById(R.id.adamotiv);
        adaGaji = view.findViewById(R.id.adagaji);


        changeStatusText();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_document, container, false);

        // Inflate the layout for this fragment
        return view;

    }


    @Override
    public void onClick(View v) {
        if(v == clickableFoto){
            if(pasFoto){
                Log.d("tags","edit foto");
                Intent intent = new Intent(getContext(), EditPasFotoActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }else{
                Log.d("tags","upload foto");
                Intent intent = new Intent(getContext(), UploadPasFotoActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }
        }else if(v == clickableTranskrip){
            if(transkrip){
                Log.d("tags","edit transkrip");
                Intent intent = new Intent(getContext(), EditTranskripNilaiActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }else{
                Log.d("tags","upload transkrip");
                Intent intent = new Intent(getContext(), UploadTranskripNilaiActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }
        }else if(v == clickableMotiv){
            if(motiv){
                Log.d("tags","edit Motiv");
                Intent intent = new Intent(getContext(), EditMotivationLetterActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }else{
                Log.d("tags","upload Motiv");
                Intent intent = new Intent(getContext(), UploadMotivationLetterActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }
        }else if (v == clickableGaji){
            if(gaji){
                Log.d("tags","edit gaji");
                Intent intent = new Intent(getContext(), EditInfoGajiActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }else{
                Log.d("tags","upload gaji");
                Intent intent = new Intent(getContext(), UploadInfoGajiActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }
        }else if (v == clickableCv){
            if(cv){
                Log.d("tags","edit cv");
                Intent intent = new Intent(getContext(), EditCvActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }else{
                Log.d("tags","upload cv");
                Intent intent = new Intent(getContext(), UploadCvActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }
        }else if (v == clickableSkor){
            if(skor){
                Log.d("tags","edit skor");
                Intent intent = new Intent(getContext(), EditSkorTOEFLActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }else{
                Log.d("tags","upload skor");
                Intent intent = new Intent(getContext(), UploadSkorTOEFLActivity.class);
                intent.putExtra("id_user",authRef.getUid());
                startActivity(intent);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        cekDokumen();
        changeStatusText();
    }


    public void cekDokumen(){
        Log.d("tags","uid :"+authRef.getUid());

        pasFotoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                pasFoto = true;
                Log.d("tags","file foto "+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                pasFoto = false;
                Log.d("Tags", exception.getMessage());
            }
        });

        transkripNilaiRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                transkrip = true;
                Log.d("tags","file transkrip;"+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                transkrip = false;
                Log.d("Tags", exception.getMessage());
            }
        });

        cvRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                cv = true;
                Log.d("tags","file cv;"+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                cv = false;
                Log.d("Tags", exception.getMessage());
            }
        });

        skorRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                skor = true;
                Log.d("tags","file skor;"+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                skor = false;
                Log.d("Tags", exception.getMessage());
            }
        });

        motivRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                motiv = true;
                Log.d("tags","file motiv;"+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                motiv = false;
                Log.d("Tags", exception.getMessage());
            }
        });

        gajiRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                gaji = true;
                Log.d("tags","file gaji;"+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                gaji = false;
                Log.d("Tags", exception.getMessage());
            }
        });

        cvRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                cv = true;
                Log.d("tags","file cv;"+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                cv = false;
                Log.d("Tags", exception.getMessage());
            }
        });

        skorRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                cv = true;
                Log.d("tags","file skor;"+uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                cv = false;
                Log.d("Tags", exception.getMessage());
            }
        });
    }

    public void changeStatusText(){
        if(pasFoto){
            adaFoto.setText("Sudah Ada");
            adaFoto.setTextColor(Color.parseColor("#00FF00"));
        }else{
            adaFoto.setText("Belum Ada");
            adaFoto.setTextColor(Color.parseColor("#FF0000"));
        }

        if(transkrip){
            adaTranskrip.setText("Sudah Ada");
            adaTranskrip.setTextColor(Color.parseColor("#00FF00"));
        }else{
            adaTranskrip.setText("Belum Ada");
            adaTranskrip.setTextColor(Color.parseColor("#FF0000"));
        }

        if(cv){
            adaCv.setText("Sudah Ada");
            adaCv.setTextColor(Color.parseColor("#00FF00"));
        }else{
            adaCv.setText("Belum Ada");
            adaCv.setTextColor(Color.parseColor("#FF0000"));
        }

        if(skor){
            adaSkor.setText("Sudah Ada");
            adaSkor.setTextColor(Color.parseColor("#00FF00"));
        }else{
            adaSkor.setText("Belum Ada");
            adaSkor.setTextColor(Color.parseColor("#FF0000"));
        }

        if(motiv){
            adaMotiv.setText("Sudah Ada");
            adaMotiv.setTextColor(Color.parseColor("#00FF00"));
        }else{
            adaMotiv.setText("Belum Ada");
            adaMotiv.setTextColor(Color.parseColor("#FF0000"));
        }

        if(gaji){
            adaGaji.setText("Sudah Ada");
            adaGaji.setTextColor(Color.parseColor("#00FF00"));
        }else{
            adaGaji.setText("Belum Ada");
            adaGaji.setTextColor(Color.parseColor("#FF0000"));
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        cekDokumen();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
