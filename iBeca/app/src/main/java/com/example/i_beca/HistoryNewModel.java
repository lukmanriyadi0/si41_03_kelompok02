package com.example.i_beca;

public class HistoryNewModel {
    private String id, title, status, imguri, jumlah;

    public HistoryNewModel(String id, String title, String status, String jumlah) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.jumlah = jumlah;
    }
    public HistoryNewModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}
