package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

public class AdminEditBeasiswaActivity extends AppCompatActivity {

    //a constant to track the file chooser intent
    private static final int PICK_IMAGE_REQUEST = 12345;

    private Uri filePath;

    ImageView imgView;

    EditText etjudul, etinstitusi, etdesc;

    Button btnback, btnupdate;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    DatabaseReference dbRef;

    String id_beasiswa;
    Boolean status;
    String downloadUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_edit_beasiswa);

        dbRef = FirebaseDatabase.getInstance().getReference();
        etjudul = findViewById(R.id.txtJudul);
        etinstitusi = findViewById(R.id.txtInstitusi);
        etdesc = findViewById(R.id.txtDeskripsi);
        btnupdate = findViewById(R.id.btnupdate);
        btnback = findViewById(R.id.btnback);

        imgView = findViewById(R.id.ImageViewEditBeasiswa);

        //get from intent
        Intent intent = getIntent();
        id_beasiswa = intent.getStringExtra("id");
        status = true;

        storageReference.child("Beasiswa/" + id_beasiswa + "/FotoBeasiswa.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                downloadUrl = uri.toString();
                // Got the download URL for 'users/me/profile.png'
                Glide.with(AdminEditBeasiswaActivity.this).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgView);
                Log.d("Test", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
            }
        });

        Log.d("tags", "berhasil ambil gambar");


        final Query selectBeasiswaById = dbRef.child("Beasiswa").orderByChild("id_beasiswa").equalTo(id_beasiswa);

        selectBeasiswaById.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    Beasiswa beasiswa = new Beasiswa();
                    for (DataSnapshot selected : dataSnapshot.getChildren()) {
                        String id = selected.child("id_beasiswa").getValue().toString();
                        String judul = selected.child("nama_beasiswa").getValue().toString();
                        String instansi = selected.child("instansi").getValue().toString();
                        String desc = selected.child("deskripsi_beasiswa").getValue().toString();
                        Boolean Status = (Boolean) selected.child("status").getValue();

                        etjudul.setText(judul);
                        etdesc.setText(desc);
                        etinstitusi.setText(instansi);
                    }


                } else {

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void openFileChooser(View view) {
        showFileChooser();
    }

    //method to show file chooser
    private void showFileChooser() {
        Log.d("tags", "pict img");
        //for greater than lolipop versions we need the permissions asked on runtime
        //so if the permission is not available user will go to the screen to allow storage permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("tags", "pict img beerhasil");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imgView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateBeasiswa() {
        String namaBeasiswa, instansi, descBeasiswa;
        namaBeasiswa = etjudul.getText().toString();
        instansi = etinstitusi.getText().toString();
        descBeasiswa = etdesc.getText().toString();

        DatabaseReference beasiswaRef = dbRef.child("Beasiswa");

        StorageReference fotoRef = storageReference.child("Beasiswa/" + id_beasiswa + "/FotoBeasiswa.jpg");

        //validasi data text


        //displaying a progress dialog while upload is going on
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading");
        progressDialog.show();

        if (filePath != null) {
            Log.d("tags", "gambar dari internal");
            final Beasiswa beasiswa = new Beasiswa(id_beasiswa, namaBeasiswa, instansi, descBeasiswa, status);
            Query update = beasiswaRef.orderByChild("id_beasiswa").equalTo(id_beasiswa);

            update.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot update : dataSnapshot.getChildren()) {

                        update.getRef().setValue(beasiswa);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            fotoRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            //and displaying a success toast
                            Toast.makeText(getApplicationContext(), "Data Uploaded ", Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //if the upload is not successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();

                            //and displaying error message
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                            //displaying percentage in progress dialog
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            Log.d("tags", "gambar masih yg tadi");
            final Beasiswa beasiswa = new Beasiswa(id_beasiswa, namaBeasiswa, instansi, descBeasiswa, status);
            Query update = beasiswaRef.orderByChild("id_beasiswa").equalTo(id_beasiswa);

            update.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot update : dataSnapshot.getChildren()) {

                        update.getRef().setValue(beasiswa);
                    }

                    progressDialog.dismiss();
                    //and displaying error message
                    Toast.makeText(getApplicationContext(), "Data Uploaded", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }


    public void clickUpdateBeasiswa(View view) {
        updateBeasiswa();
    }
}
