package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class Detail_beasiswa extends AppCompatActivity {
    String id,Title,institusi,desc;
    TextView tv_nama,tv_desc;
    ImageView iv_utama;
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;
    boolean status;
    boolean statusFoto = false;
    boolean statusTranskrip = false;
    boolean statusCv = false;
    boolean statusMotiv = false;
    boolean statusGaji = false;
    boolean statusSkor = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_beasiswa);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        Title=intent.getStringExtra("nama");
        institusi=intent.getStringExtra("instansi");
        desc=intent.getStringExtra("desc");
        id=intent.getStringExtra("id");

        tv_nama=findViewById(R.id.txtJudul);
        tv_nama.setText(Title);

//        tv_institusi=findViewById(R.id.txtInstitusi);
//        tv_institusi.setText(institusi);

        tv_desc=findViewById(R.id.txtDeskripsi);
        tv_desc.setText(desc);

        iv_utama=findViewById(R.id.imgHeader);

        storageReference.child("Beasiswa/"+id+"/FotoBeasiswa.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                Glide.with(Detail_beasiswa.this).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(iv_utama);
                Log.d("Test",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        firebaseUser = firebaseAuth.getCurrentUser();

        try{
            firebaseUser.getUid();
        }catch(NullPointerException ex){
            ex.printStackTrace();
        }
        cekCv();
        cekFoto();
        cekMotiv();
        cekSkor();
        cekTranskrip();
        cekGaji();
        cekSkor();
        cekCv();
    }

    public void doApply(View view){
        ApplyBeasiswa applyBeasiswa = new ApplyBeasiswa(id, firebaseUser.getUid(), "Pending");
        processApply(applyBeasiswa);
    }

    private void processApply(ApplyBeasiswa applyBeasiswa){
        try{

            if(statusFoto != true || statusTranskrip != true || statusCv != true || statusMotiv != true || statusGaji != true || statusSkor != true){
                Toast.makeText(this, "File dokumen belum lengkap, silahkan lengkapi terlebih dahulu", Toast.LENGTH_LONG).show();
                return;
            }

            databaseReference.child("Transaksi").push().setValue(applyBeasiswa)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(Detail_beasiswa.this, "Berhasil apply beasiswa", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                    }else{
                        Toast.makeText(Detail_beasiswa.this, "Gagal apply beasiswa", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

//    private boolean cekDokumen(){
////        try{
////            storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/Filefoto.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
////                @Override
////                public void onSuccess(Uri uri) {
////                    // Got the download URL for 'users/me/profile.png'
////                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
////                            .fitCenter() // menyesuaikan ukuran imageview
////                            .diskCacheStrategy(DiskCacheStrategy.ALL)
////                            .into(iv_utama);*/
////                    status = true;
////                    Log.d("File Foto: ",uri.toString());
////                }
////            }).addOnFailureListener(new OnFailureListener() {
////                @Override
////                public void onFailure(@NonNull Exception exception) {
////                    status = false;
////                    Log.d("Tags", exception.getMessage());
////                }
////            });
////
////            storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/filetranskrip.pdf").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
////                @Override
////                public void onSuccess(Uri uri) {
////                    // Got the download URL for 'users/me/profile.png'
////                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
////                            .fitCenter() // menyesuaikan ukuran imageview
////                            .diskCacheStrategy(DiskCacheStrategy.ALL)
////                            .into(iv_utama);*/
////                    status = true;
////                    Log.d("File Transkrip",uri.toString());
////                }
////            }).addOnFailureListener(new OnFailureListener() {
////                @Override
////                public void onFailure(@NonNull Exception exception) {
////                    status = false;
////                    Log.d("Tags", exception.getMessage());
////                }
////            });
////
////            storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/filecv.pdf").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
////                @Override
////                public void onSuccess(Uri uri) {
////                    // Got the download URL for 'users/me/profile.png'
////                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
////                            .fitCenter() // menyesuaikan ukuran imageview
////                            .diskCacheStrategy(DiskCacheStrategy.ALL)
////                            .into(iv_utama);*/
////                    status = true;
////                    Log.d("File CV",uri.toString());
////                }
////            }).addOnFailureListener(new OnFailureListener() {
////                @Override
////                public void onFailure(@NonNull Exception exception) {
////                    status = false;
////                    Log.d("Tags", exception.getMessage());
////                }
////            });
////
////            storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/fileskor.pdf").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
////                @Override
////                public void onSuccess(Uri uri) {
////                    // Got the download URL for 'users/me/profile.png'
////                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
////                            .fitCenter() // menyesuaikan ukuran imageview
////                            .diskCacheStrategy(DiskCacheStrategy.ALL)
////                            .into(iv_utama);*/
////                    status = true;
////                    Log.d("File Skor",uri.toString());
////                }
////            }).addOnFailureListener(new OnFailureListener() {
////                @Override
////                public void onFailure(@NonNull Exception exception) {
////                    status = false;
////                    Log.d("Tags", exception.getMessage());
////                }
////            });
////        }catch (Exception ex){
////            ex.printStackTrace();
////        }
////
////        return status;
////    }

    private void cekFoto(){
        Log.d("tags","Status Foto Lama : "+statusFoto);
        storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/Filefoto.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                statusFoto = true;
                Log.d("Tags","Status Foto Sekarang :"+statusFoto);
                Log.d("File Foto: ",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
                statusFoto = false;
                Log.d("Tags","Status Foto Sekarang :"+statusFoto);
            }
        });
    }

    private void cekTranskrip(){
        Log.d("tags","Status Transkrip Lama : "+statusTranskrip);
        storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/filetranskrip.pdf").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                statusTranskrip = true;
                Log.d("Tags","Status Transkrip Sekarang :"+statusTranskrip);
                Log.d("File Transkrip",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
                statusTranskrip = false;
                Log.d("Tags","Status Transkrip Sekarang :"+statusTranskrip);

            }
        });
    }

    private void cekCv(){
        Log.d("tags","Status CV Lama : "+statusCv);
        storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/filecv.pdf").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                statusCv = true;
                Log.d("Tags","Status Cv Sekarang :"+statusCv);
                Log.d("File CV",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                statusCv = false;
                Log.d("Tags","Status Cv Sekarang :"+statusCv);
                Log.d("Tags", exception.getMessage());
            }
        });
    }

    private void cekSkor(){
        Log.d("tags","Status Skor Lama : "+statusSkor);
        storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/fileskor.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                statusSkor = true;
                Log.d("Tags","Status Skor Sekarang :"+statusSkor);
                Log.d("File Skor",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                statusSkor = false;
                Log.d("Tags","Status Skor Sekarang :"+statusSkor);
                Log.d("Tags", exception.getMessage());
            }
        });
    }

    private void cekMotiv(){
        Log.d("tags","Status Motiv Lama : "+statusMotiv);
        storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/filemotiv.pdf").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                statusMotiv = true;
                Log.d("Tags","Status Motiv Sekarang :"+statusMotiv);

                Log.d("File Motiv",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                statusSkor = false;
                Log.d("Tags","Status Motiv Sekarang :"+statusMotiv);
                Log.d("Tags", exception.getMessage());
            }
        });
    }

    private void cekGaji(){
        Log.d("tags","Status Gaji Lama : "+statusGaji);
        storageReference.child("Mahasiswa/"+firebaseUser.getUid()+"/filegaji.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                    /*Glide.with(Detail_beasiswa.this).load(uri.toString())
                            .fitCenter() // menyesuaikan ukuran imageview
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv_utama);*/
                statusGaji = true;
                Log.d("Tags","Status Gaji Sekarang :"+statusGaji);

                Log.d("File Motiv",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                statusGaji = false;
                Log.d("Tags","Status Gaji Sekarang :"+statusGaji);
                Log.d("Tags", exception.getMessage());
            }
        });

    }





    class ApplyBeasiswa{
        private String id_beasiswa, id_mahasiswa, status;

        public ApplyBeasiswa(){}

        public ApplyBeasiswa(String id_beasiswa, String id_mahasiswa, String status) {
            this.id_beasiswa = id_beasiswa;
            this.id_mahasiswa = id_mahasiswa;
            this.status = status;
        }

        public String getId_beasiswa() {
            return id_beasiswa;
        }

        public void setId_beasiswa(String id_beasiswa) {
            this.id_beasiswa = id_beasiswa;
        }

        public String getId_mahasiswa() {
            return id_mahasiswa;
        }

        public void setId_mahasiswa(String id_mahasiswa) {
            this.id_mahasiswa = id_mahasiswa;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
