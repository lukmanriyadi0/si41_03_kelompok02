package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NotifActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private NotifAdapter adapter;
    public ArrayList<NotifModel> NotifArrayList;
    private GridLayoutManager setLayoutManager;
    private ImageView back;
    public String id_beasiswa;
    int count=0;
    public String id_user;
    DatabaseReference dbRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif);
        //back arrow
        back=(ImageView)findViewById(R.id.imageView4);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(),MainActivity.class);
                startActivity(intent);

            }
        });

        ///notif
        dbRef = FirebaseDatabase.getInstance().getReference();
        NotifArrayList = new ArrayList<>();
        id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String currentuser = id_user;
        recyclerView = (RecyclerView) findViewById(R.id.notif);



        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NotifActivity.this);

        recyclerView.setLayoutManager(layoutManager);


        final Query selectnotifBymhs_id = dbRef.child("Transaksi").orderByChild("id_mahasiswa").equalTo(currentuser);

        selectnotifBymhs_id.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            ////untuk order by id mahasiswa
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    Log.d("Tags","bisa select transaksi");
                    for (DataSnapshot selected : dataSnapshot.getChildren()){
                        Log.d("Tags","data transaksi ke "+count);
                        id_beasiswa = selected.child("id_beasiswa").getValue().toString();
                        final String status = selected.child("status").getValue().toString();
                        count=count+1;

                        //// jika Tolak atau Terima lanjut ke beasiswa
                        if(status.equals("Tolak")||status.equals("Terima")){
                            Log.d("Tags","id:beasiswa = "+id_beasiswa);
                            Log.d("Tags","ditolak atau terima");
                            final Query selectBeasiswaById = dbRef.child("Beasiswa").orderByChild("id_beasiswa").equalTo(id_beasiswa);

                            selectBeasiswaById.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    Log.d("Tags","ngambil data beasiswa");
                                    if(dataSnapshot.exists()){
                                        Log.d("Tags","data beasiswa ada");
                                        for (DataSnapshot selected : dataSnapshot.getChildren()){
                                            Log.d("Tags","data beasiswa ke");
                                            String id = selected.child("id_beasiswa").getValue().toString();
                                            String judul = selected.child("nama_beasiswa").getValue().toString();
                                            String instansi = selected.child("instansi").getValue().toString();
                                            String desc  = selected.child("deskripsi_beasiswa").getValue().toString();
                                            Boolean Status = (Boolean) selected.child("status").getValue();



                                            NotifArrayList.add(new NotifModel(id, judul, status));
                                            Log.d("Tagtes",id+judul+status);
//                                            NotifArrayList.add(new NotifModel("id_beasiswa", "judul", "Lolos"));
                                            adapter.notifyDataSetChanged();
                                        }



                                    }else{
                                        Log.d("Tags","data beasiswa(lloping kedua ga ada");
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }


                    }

                    adapter = new NotifAdapter(NotifArrayList);
                    recyclerView.setAdapter(adapter);
                }else{

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


//        addData();

    }



//    private void addData() {
//
//        NotifArrayList.add(new NotifModel("2", "ABCABCABC ", "Maaf anda tika lolos pada beasiswa ini"));
//
//
//
//    }
}