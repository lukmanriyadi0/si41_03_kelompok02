package com.example.i_beca;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends AppCompatActivity {
    FirebaseAuth authRef;
    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        authRef = FirebaseAuth.getInstance();
        sharedPref = new SharedPref(this);

        if (authRef.getCurrentUser() != null && sharedPref.getBoolean(SharedPref.formatKey(SharedPref.REMEMBER,authRef.getCurrentUser().getUid()))) {
            authRef.signInWithEmailAndPassword(sharedPref.getString(SharedPref.EMAIL), sharedPref.getString(SharedPref.PASSWORD))
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            if(sharedPref.getString(SharedPref.EMAIL).equals("admin@ibeca.com")){
                                Toast.makeText(getApplicationContext(), "Berhasil Login!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MainAdminActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(getApplicationContext(), "Berhasil Login!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }
                        }
                    });
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, LoginsignupActivity.class));
                    finish();

                }
            },2000);
        }


    }
}
