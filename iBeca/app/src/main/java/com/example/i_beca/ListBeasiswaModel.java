package com.example.i_beca;

public class ListBeasiswaModel {


    private int id;
    private String title, institusi, desc, imguri;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstitusi() {
        return institusi;
    }

    public void setInstitusi(String institusi) {
        this.institusi = institusi;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImguri() {
        return imguri;
    }

    public void setImguri(String imguri) {
        this.imguri = imguri;
    }

    public ListBeasiswaModel() {

    }

    public ListBeasiswaModel(int id, String title, String institusi, String desc, String imguri) {
        this.id = id;
        this.title = title;
        this.institusi = institusi;
        this.desc = desc;
        this.imguri = imguri;
    }
}
