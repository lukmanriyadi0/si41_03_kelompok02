package com.example.i_beca;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class ListMahasiswaAdapter extends RecyclerView.Adapter<ListMahasiswaAdapter.ListViewHolder> {
    private ArrayList<ListMahasiswaModel> mListMahasiswa;
    public static class ListViewHolder extends RecyclerView.ViewHolder{
        private final Context context;
        public CardView mCardView;
        public ImageView mImageView;
        public TextView mTextDate;
        public TextView mTextName;
        public TextView mTextBea;
        public TextView mTextStatus;
        public TextView mTanggalLahir;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            mCardView = itemView.findViewById(R.id.cardDetail);
            mImageView = itemView.findViewById(R.id.imageViewListMahasiswa);
            mTextDate = itemView.findViewById(R.id.textDate);
            mTextName = itemView.findViewById(R.id.textNama);
            mTextBea = itemView.findViewById(R.id.textBeasiswa);
            mTextStatus = itemView.findViewById(R.id.textStatus);
            mTanggalLahir = itemView.findViewById(R.id.textTanggalLahir);
        }
    }

    public ListMahasiswaAdapter(ArrayList<ListMahasiswaModel> listMahasiswa){
           mListMahasiswa = listMahasiswa;
    }


    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_mahasiswa, parent, false);
        ListViewHolder lvh = new ListViewHolder(v);
        return lvh;
    }
    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {

        final ListMahasiswaModel currentItem = mListMahasiswa.get(position);
        holder.mImageView.setImageResource(R.drawable.blank_image);
//        holder.mTextDate.setText(currentItem.getTextDate());
        holder.mTextName.setText(currentItem.getNama_mahasiswa());
//        holder.mTextBea.setText(currentItem.getTextBea());
//        holder.mTextStatus.setText(currentItem.getTextStatus());
        holder.mTanggalLahir.setText(currentItem.getTanggal_lahir());

        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Snackbar.make(view,"NAJENCCCCCCCCC",10).show();

                Intent intent= new Intent(view.getContext(), DetailMahasiswaApply.class);
                intent.putExtra("id_mahasiswa", currentItem.getId_mahasiswa());
                intent.putExtra("id_beasiswa", currentItem.getId_beasiswa());
                intent.putExtra("nama_mahasiswa",currentItem.getNama_mahasiswa());
                view.getContext().startActivity(intent);
//                startActivity(new Intent (v.getContext(), OnCardSelected.class);)

        }
        });
    }

    @Override
    public int getItemCount() {
        return mListMahasiswa.size();
    }

}
