package com.example.i_beca;

import java.util.List;

public class ListMahasiswaModel {
    private int mImageResource;
    private String mTextDate,mTextName,mTextBea,mTextStatus;
    private String id_mahasiswa,nama_mahasiswa,tanggal_lahir,id_beasiswa;
    public ListMahasiswaModel (String textDate,int imageResource,String textName,String textBea,String textStatus){
        mImageResource = imageResource;
        mTextDate = textDate;
        mTextName = textName;
        mTextBea = textBea;
        mTextStatus = textStatus;
    }

    public ListMahasiswaModel (String id_mahasiswa, String nama_mahasiswa, String tanggal_lahir,String id_beasiswa){
        this.id_mahasiswa = id_mahasiswa;
        this.nama_mahasiswa = nama_mahasiswa;
        this.tanggal_lahir = tanggal_lahir;
        this.id_beasiswa = id_beasiswa;
    }

    public String getId_beasiswa() {
        return id_beasiswa;
    }

    public void setId_beasiswa(String id_beasiswa) {
        this.id_beasiswa = id_beasiswa;
    }

    public String getId_mahasiswa() {
        return id_mahasiswa;
    }

    public void setId_mahasiswa(String id_mahasiswa) {
        this.id_mahasiswa = id_mahasiswa;
    }

    public String getNama_mahasiswa() {
        return nama_mahasiswa;
    }

    public void setNama_mahasiswa(String nama_mahasiswa) {
        this.nama_mahasiswa = nama_mahasiswa;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public int getImageResource() {
        return mImageResource;
    }

    public String getTextDate(){
        return mTextDate;
    }

    public String getTextName(){
        return mTextName;
    }

    public String getTextBea(){
        return mTextBea;
    }

    public String getTextStatus(){
        return mTextStatus;
    }

}
