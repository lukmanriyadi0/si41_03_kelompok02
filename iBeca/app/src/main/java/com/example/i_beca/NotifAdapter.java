package com.example.i_beca;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.ViewHolder>  {
    private ArrayList<NotifModel> notif;

    public NotifAdapter(ArrayList<NotifModel> notif) {
        this.notif = notif;
    }
    @NonNull
    @Override
    public NotifAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_notif, parent, false);
            return new NotifAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotifAdapter.ViewHolder holder, int position) {
            holder.bind(notif.get(position));


    }

    @Override
    public int getItemCount() {
        return notif.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvtitle, tvsubtitle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvtitle = itemView.findViewById(R.id.notifTitle);
            tvsubtitle = itemView.findViewById(R.id.notifText);
            itemView.setOnClickListener(this);
        }
        public void bind(NotifModel notifModel) {
            tvtitle.setText(notifModel.getTitle());
            Log.d("Tags","Status :"+notifModel.getSubtitle());
            if(notifModel.getSubtitle().equalsIgnoreCase("Tolak")){

                tvsubtitle.setText("Mohon maaf kamu tidak lolos pada beasiswa " + notifModel.getTitle());
            }else if(notifModel.getSubtitle().equalsIgnoreCase("Terima")){

                tvsubtitle.setText("Selamat kamu lolos pada beasiswa " + notifModel.getTitle());
            }
            else {

                tvsubtitle.setText("Mahasiswa " + notifModel.getTitle()+" melakukan apply");
            }


        }

        @Override
        public void onClick(View view) {
            NotifModel notifModel = notif.get(getAdapterPosition());

            Intent intent = new Intent(view.getContext(), DetailNotifActivity.class);
            intent.putExtra("Id", notifModel.getId());
            intent.putExtra("Status", notifModel.getSubtitle());
            view.getContext().startActivity(intent);

        }
    }
}

