package com.example.i_beca;

import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;

public class ProgressBarIbeca {
    private static final String TAG = "ProgressBarIbeca";

    Activity activity;
    AlertDialog alertDialog;

    public ProgressBarIbeca(Activity activity) {
        this.activity = activity;
    }



    public void startProgressBarIbeca() {
        Log.d(TAG, "startProgressbar: "+activity);

        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);

        LayoutInflater inflater = this.activity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.progress_bar, null));
        builder.setCancelable(false);

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void endProgressBarIbeca() {
        alertDialog.dismiss();
        Log.d(TAG, "endProgressbar: "+activity);
    }
}
