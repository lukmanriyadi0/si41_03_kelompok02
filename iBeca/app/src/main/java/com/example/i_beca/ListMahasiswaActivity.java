package com.example.i_beca;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListMahasiswaActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    TextView txtTitleDaftarPendaftar;
    String id_beasiswa,nama_beasiswa;

    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mahasiswaRef = databaseReference.child("Mahasiswa");
    DatabaseReference beasiswaRef = databaseReference.child("Beasiswa");
    DatabaseReference transaksiRef = databaseReference.child("Transaksi");

    ArrayList<ListMahasiswaModel> mhsList = new ArrayList<>();
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listmahasiswa);
        Intent intent = getIntent();
        id_beasiswa = intent.getStringExtra("id_beasiswa");
        nama_beasiswa = intent.getStringExtra("nama_beasiswa");
        txtTitleDaftarPendaftar = findViewById(R.id.textViewTitleDaftarPendaftar);
        txtTitleDaftarPendaftar.setText("Daftar Pendaftar "+nama_beasiswa);

        Query getListMhs = transaksiRef.orderByChild("id_beasiswa").equalTo(id_beasiswa);
        getListMhs.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.d("tags","berhasil ambil id pendaftar");
                    for (DataSnapshot mhs : dataSnapshot.getChildren()) {
                        Log.d("tags","id pendaftar ke ");
                        // do something with the individual "issues"
                        String id_mhs = mhs.child("id_mahasiswa").getValue().toString();
                        Log.d("tags","id_mhs"+id_mhs);

                        Query getdDetailMhs = mahasiswaRef.orderByChild("id").equalTo(id_mhs);

                        getdDetailMhs.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    Log.d("tags","berhasil ambil detail pendaftar");

                                    for(DataSnapshot detail : dataSnapshot.getChildren()){
                                     Log.d("tags","detail pendaftar ke");
                                        String id = detail.child("id").getValue().toString();
                                        String nama = detail.child("nama").getValue().toString();
                                        String tanggal = detail.child("tanggal").getValue().toString();

                                        ListMahasiswaModel mhs = new ListMahasiswaModel(id,nama,tanggal,id_beasiswa);
                                        mhsList.add(mhs);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }else{
                                    Log.d("tags","data detail tidak ada");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }else{
                    Log.d("tags","data id pendaftar tidak ditemukan");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        ArrayList<ListMahasiswaModel>  listMahasiswa = new ArrayList<>();
        listMahasiswa.add(new ListMahasiswaModel("27 April 2020", R.drawable.blank_image, "Satriadi", "Mandiri", "Apply"));

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ListMahasiswaAdapter(mhsList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }


}
