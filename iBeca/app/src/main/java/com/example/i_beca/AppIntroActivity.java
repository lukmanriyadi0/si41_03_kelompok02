package com.example.i_beca;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Toast;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources res = getResources();
        String[] descriptions = res.getStringArray(R.array.description_intro);
        String[] titles = res.getStringArray(R.array.title_intro);

        addSlide(AppIntroFragment
                .newInstance(titles[0],descriptions[0],R.drawable.pict_intro_1,
                        ContextCompat.getColor(getApplicationContext(),R.color.colorBackgroundIntro),
                        ContextCompat.getColor(getApplicationContext(),R.color.colorTitleIntro),
                        ContextCompat.getColor(getApplicationContext(),R.color.colorDescriptionIntro)));
        addSlide(AppIntroFragment
                .newInstance(titles[1],descriptions[1],R.drawable.pict_intro_2,
                        ContextCompat.getColor(getApplicationContext(),R.color.colorBackgroundIntro),
                        ContextCompat.getColor(getApplicationContext(),R.color.colorTitleIntro),
                        ContextCompat.getColor(getApplicationContext(),R.color.colorDescriptionIntro)));
        addSlide(AppIntroFragment
                .newInstance(titles[2],descriptions[2],R.drawable.pict_intro_3,
                        ContextCompat.getColor(getApplicationContext(),R.color.colorBackgroundIntro),
                        ContextCompat.getColor(getApplicationContext(),R.color.colorTitleIntro),
                        ContextCompat.getColor(getApplicationContext(),R.color.colorDescriptionIntro)));



        setDoneText("Mulai");
        setColorDoneText(ContextCompat.getColor(getApplicationContext(),R.color.colorBackgroundIntro));
        setSkipText("Skip");
        setColorSkipButton(ContextCompat.getColor(getApplicationContext(),R.color.colorBackgroundIntro));
        setBarColor(ContextCompat.getColor(getApplicationContext(),R.color.colorTitleIntro));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

}
