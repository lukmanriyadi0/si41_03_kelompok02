package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class DetailBeasiswaAdmin extends AppCompatActivity {
    String id, Title, institusi, desc;
    TextView tv_nama, tv_desc;
    ImageView iv_utama;
    TextView pendaftar,lihatpendaftar;
    int jumlahPendaftar;
    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_beasiswa_admin);
        Intent intent = getIntent();
        Title = intent.getStringExtra("nama");
        institusi = intent.getStringExtra("instansi");
        desc = intent.getStringExtra("desc");
        id = intent.getStringExtra("id");

        tv_nama = findViewById(R.id.txtJudul);
        tv_nama.setText(Title);

//        tv_institusi=findViewById(R.id.txtInstitusi);
//        tv_institusi.setText(institusi);

        tv_desc = findViewById(R.id.txtDeskripsi);
        tv_desc.setText(desc);

        iv_utama = findViewById(R.id.imgHeader);




        pendaftar = findViewById(R.id.pendaftar);
        Log.d("Tags","Pendaftar : "+jumlahPendaftar);
        lihatpendaftar = findViewById(R.id.LihatPendaftar);
        //ngambil data pendaftar
        Query transaksi = myRef.child("Transaksi").orderByChild("id_beasiswa").equalTo(id);
        transaksi.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    jumlahPendaftar = (int) dataSnapshot.getChildrenCount();
                    Log.d("Tags"," set Pendaftar : "+jumlahPendaftar);
                    pendaftar.setText(jumlahPendaftar+" Pendaftar");
                }else{
                    Log.d("tags","ga ada pendaftar");
                    pendaftar.setText(0+" Pendaftar");
                    lihatpendaftar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showMessage("Eror cuy", databaseError.getMessage());
            }
        });


        storageReference.child("Beasiswa/" + id + "/FotoBeasiswa.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                Glide.with(DetailBeasiswaAdmin.this).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(iv_utama);
                Log.d("Test", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
            }
        });
    }

    public void EditBeasiswa(View view) {
        Intent intent = new Intent(this, AdminEditBeasiswaActivity.class);
        intent.putExtra("id", id);
        this.startActivity(intent);
    }

    public void DeleteBeasiswa(View view) {
        Query del = myRef.child("Beasiswa").orderByChild("id_beasiswa").equalTo(id);
        del.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot DSnote : dataSnapshot.getChildren()) {
                    DSnote.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showMessage("Eror cuy", databaseError.getMessage());
            }

        });
    }

    public void InactiveBeasiswa(View view) {
        final Beasiswa beasiswa = new Beasiswa(id, Title, institusi, desc, false);

        Query update = myRef.child("Beasiswa").orderByChild("id_beasiswa").equalTo(id);
        update.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot DSnote : dataSnapshot.getChildren()) {
                    DSnote.getRef().setValue(beasiswa);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showMessage("Eror cuy", databaseError.getMessage());
            }
        });
    }

    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void LihatPendaftar(View view) {
        Intent intent = new Intent(this, ListMahasiswaActivity.class);
        intent.putExtra("id_beasiswa", id);
        intent.putExtra("nama_beasiswa",Title);
        intent.putExtra("testing","testing");
        startActivity(intent);

    }
}
