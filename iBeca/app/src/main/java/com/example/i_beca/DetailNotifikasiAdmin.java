package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class DetailNotifikasiAdmin extends AppCompatActivity {

    private String id_beasiswa, id_user, status, namaMahasiswa, judulBeasiswa;
    TextView nama, stts, bodi;
    ImageView profPic;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    String downloadUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_notifikasi_admin);

        Intent intent = getIntent();
        id_beasiswa = intent.getStringExtra("id_user");
        id_user=intent.getStringExtra("Id");
        status=intent.getStringExtra("Status");
        Log.d("Tags",id_user+id_beasiswa+status);
        nama=findViewById(R.id.txtName);
        stts=findViewById(R.id.txtStatus);
        bodi=findViewById(R.id.txtBodi);
        profPic=findViewById(R.id.profile_image);

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();

        final Query selectBeasiswaById = dbRef.child("Beasiswa").orderByChild("id_beasiswa").equalTo(id_beasiswa);
        selectBeasiswaById.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){

                    for (DataSnapshot selected : dataSnapshot.getChildren()){
                        judulBeasiswa = selected.child("nama_beasiswa").getValue().toString();

                        bodi.setText("telah mengajukan pendaftaran beasiswa untuk beasiswa "+judulBeasiswa+". Silahkan klik Detail Mahasiswa untuk melihat dokumen-dokumen mahasiswa");
                    }

                }else{ }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final Query selectMahasiswaById = dbRef.child("Mahasiswa").orderByChild("id").equalTo(id_user);
        selectMahasiswaById.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("Tags","ngambil datamaha");
                if(dataSnapshot.exists()){
                    for (DataSnapshot selected : dataSnapshot.getChildren()){
                        namaMahasiswa = selected.child("nama").getValue().toString();
                        Log.d("Tags",namaMahasiswa+status);
                        nama.setText(namaMahasiswa);
                        stts.setText(status);
                    }

                }else{ }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        storageReference.child("Mahasiswa/"+id_user+"/Filefoto.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                downloadUrl = uri.toString();
                // Got the download URL for 'users/me/profile.png'
                Glide.with(DetailNotifikasiAdmin.this).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(profPic);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        });

        Log.d("tags","berhasil ambil gambar");


    }
}
