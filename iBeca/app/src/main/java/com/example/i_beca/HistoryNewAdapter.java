package com.example.i_beca;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class HistoryNewAdapter extends RecyclerView.Adapter<HistoryNewAdapter.MyViewHolder> {
    private List<HistoryNewModel> Historys;
    private Context context;
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    public HistoryNewAdapter(List<HistoryNewModel> listhistory, Context context) {
        this.Historys = listhistory;
        this.context = context;
    }



    @NonNull
    @Override
    public HistoryNewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_history, parent, false);
        HistoryNewAdapter.MyViewHolder mViewHolder = new HistoryNewAdapter.MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryNewAdapter.MyViewHolder holder, int position) {
        HistoryNewModel HistoryPosition = Historys.get(position);
        final String id_beasiswa = String.valueOf(HistoryPosition.getId());
        final String status = String.valueOf(HistoryPosition.getStatus());
        final String Title =  String.valueOf(HistoryPosition.getTitle());
//        final String jumlah = String.valueOf(HistoryPosition.getJumlah());
        holder.mTitle.setText(Title);

        holder.mId = id_beasiswa;

        if(status.equalsIgnoreCase("Terima")){
            holder.status.setText(status);
            holder.status.setTextColor(Color.parseColor("#41967D"));
        }
        else if(status.equalsIgnoreCase("Tolak")){
            holder.status.setText(status);
            holder.status.setTextColor(Color.parseColor("#FD5957"));
        }
        else{
            holder.status.setText(status);
        }
//        holder.jumlah.setText(jumlah);

        storageReference.child("Beasiswa/"+id_beasiswa+"/FotoBeasiswa.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String downloadUrl = uri.toString();
                // Got the download URL for 'users/me/profile.png'
                Glide.with(context).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.mImageView);
                Log.d("Test",uri.toString());

            }
        });

    }

    @Override
    public int getItemCount() {
        return Historys.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;
        TextView mTitle;
        TextView status;
//        TextView jumlah;
        String mId;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageHistory);
            mTitle = itemView.findViewById(R.id.textBeasiswa);
            status = itemView.findViewById(R.id.textStatus);
//            jumlah = itemView.findViewById(R.id.textParticipant);
        }
    }
}
