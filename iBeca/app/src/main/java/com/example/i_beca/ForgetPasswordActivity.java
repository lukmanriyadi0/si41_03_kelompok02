package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ForgetPasswordActivity extends AppCompatActivity {
    EditText etnewpassword,etnewpasswordrepeat;
    FirebaseAuth authRef;
    DatabaseReference databaseRef;
    String id,nama,tanggal,email,password,keamanan,jawaban;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        etnewpassword = findViewById(R.id.et_new_password);
        etnewpasswordrepeat = findViewById(R.id.et_new_password_repeat);


        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        nama = bundle.getString("nama");
        tanggal = bundle.getString("tanggal");
        email = bundle.getString("email");
        password = bundle.getString("password");
        keamanan = bundle.getString("keamanan");
        jawaban = bundle.getString("jawaban");
        Log.d("Tags","email :"+email);
        Log.d("Tags","password:"+password);
        authRef = FirebaseAuth.getInstance();
        databaseRef = FirebaseDatabase.getInstance().getReference();

    }

    public void resetPassword(View view) {
        final String newPassword = etnewpassword.getText().toString();
        String newPasswordRepeat = etnewpasswordrepeat.getText().toString();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.show();
        if(!newPassword.equals(newPasswordRepeat)){
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(),"Password Tidak Cocok",Toast.LENGTH_LONG);
            return;
        }else{


            authRef.signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){

                                Log.d("Tags","berhasil login");
                                authRef.getCurrentUser().updatePassword(newPassword).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {


                                        Log.d("Tags","berhasil Ganti password di auth");
                                        //ganti password di realtime database
                                        Query updatePassword = databaseRef.child("Mahasiswa").orderByChild("id").equalTo(id);
                                        updatePassword.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                for (DataSnapshot updated : dataSnapshot.getChildren()){
                                                    Mahasiswa mhs = new Mahasiswa(id,nama,tanggal,email,newPassword,keamanan,jawaban);
                                                    updated.getRef().setValue(mhs);
                                                }
                                                Log.d("Tags","berhasil update password di database");
                                                progressDialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "Sukses Reset Password, Silahkan Login", Toast.LENGTH_LONG).show();

                                                Intent intent = new Intent(ForgetPasswordActivity.this, LoginsignupActivity.class);
                                                startActivity(intent);

                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                progressDialog.dismiss();

                                                Log.d("Tags","gagal update data password di database"+databaseError.getMessage());
                                            }
                                        });
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d("Tags","gagal ganti password"+e.getMessage());
                                    }
                                });
                            }else{
                                Log.d("Tags","gagal Login"+task.getException());
                            }
                        }
                    });
        }
    }
}
