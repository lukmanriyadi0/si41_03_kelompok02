package com.example.i_beca;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static String EMAIL = "email";
    public static String PASSWORD = "password";
    public static String REMEMBER = "remember";

    public SharedPref(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("I-Beca",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static String formatKey(String key, String userUID) {
        return key+"-"+userUID;
    }

    public void setString(String key, String value) {
        editor.putString(key,value);
        editor.apply();
    }

    public void setBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key,"");
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key,false);
    }

    public void clearCredential(String userUID) {
        editor.remove(EMAIL);
        editor.remove(PASSWORD);
        editor.remove(formatKey(REMEMBER, userUID));
        editor.apply();
    }
}
