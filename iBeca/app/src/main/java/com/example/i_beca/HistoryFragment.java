package com.example.i_beca;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private HistoryNewAdapter adapter;
    private List<HistoryNewModel> historyArrayList;
    DatabaseReference dbRef;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private Query query;
    public String id_user;
    public String id_beasiswa;
    private int count;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Loading..");
        progressDialog.show();
        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        historyArrayList = new ArrayList<>();

        id_user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String currentuser = id_user;
        dbRef = FirebaseDatabase.getInstance().getReference();
        final Query selecthistory = dbRef.child("Transaksi").orderByChild("id_mahasiswa").equalTo(currentuser);

        selecthistory.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot selected : dataSnapshot.getChildren()) {
                    Log.d("Step ", "loop 1");

                    id_beasiswa = selected.child("id_beasiswa").getValue().toString();
                    final String status = selected.child("status").getValue().toString();


//                    final Query selectTransaksiById = dbRef.child("Transaksi").orderByChild("id_beasiswa").equalTo(id_beasiswa);
//                    selectTransaksiById.addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            if (dataSnapshot.exists()) {
//                                for (DataSnapshot selected : dataSnapshot.getChildren()) {
//                                    Log.d("Step ", "loop 2");
//                                    count = (int) dataSnapshot.getChildrenCount();
//                                }
//                                Log.d("jumlah", String.valueOf(count));
//
//
//                            } else {
//                                Log.d("Tags", "data beasiswa(lloping kedua ga ada");
//                            }
//
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//                    });


                    final Query selectBeasiswaById = dbRef.child("Beasiswa").orderByChild("id_beasiswa").equalTo(id_beasiswa);
                    selectBeasiswaById.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                            if (dataSnapshot.exists()) {
                                for (DataSnapshot selected : dataSnapshot.getChildren()) {
                                    Log.d("Step ", "loop 3");
                                    final int jumlah = count;

                                    String id = selected.child("id_beasiswa").getValue().toString();
                                    String judul = selected.child("nama_beasiswa").getValue().toString();


                                    historyArrayList.add(new HistoryNewModel(id, judul, status, String.valueOf(jumlah)));

                                    adapter.notifyDataSetChanged();
                                }


                            } else {

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
                adapter = new HistoryNewAdapter(historyArrayList, getContext());
                recyclerView.setAdapter(adapter);
                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_history, container, false);
    }
}
