package com.example.i_beca;

public class Mahasiswa {
    String id;
    String nama;
    String tanggal;
    String email;
    String password;
    String keamanan;
    String jawaban;


    public Mahasiswa() {
    }

    public Mahasiswa(String id, String nama, String tanggal, String email, String password, String keamanan, String jawaban) {
        this.id = id;
        this.nama = nama;
        this.tanggal = tanggal;
        this.email = email;
        this.password = password;
        this.keamanan = keamanan;
        this.jawaban = jawaban;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKeamanan() {
        return keamanan;
    }

    public void setKeamanan(String keamanan) {
        this.keamanan = keamanan;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }
}
