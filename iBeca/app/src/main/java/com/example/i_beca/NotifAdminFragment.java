package com.example.i_beca;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotifAdminFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotifAdminFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private NotifAdminAdapter adapter;
    public ArrayList<NotifModel> NotifArrayList;
    public String id_beasiswa;
    public String id_mahasiswa;
    public String status;
    int count=0;
    public String id_user;
    DatabaseReference dbRef;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        dbRef = FirebaseDatabase.getInstance().getReference();
        NotifArrayList = new ArrayList<>();
        recyclerView =(RecyclerView)getView().findViewById(R.id.notif);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        final Query selectnotifByStatus = dbRef.child("Transaksi").orderByChild("status").equalTo("Pending");
        selectnotifByStatus.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.d("Tags","bisa select transaksi");
                    for (DataSnapshot selected : dataSnapshot.getChildren()){
                        Log.d("Tags","data transaksi ke "+count);
                        id_beasiswa = selected.child("id_beasiswa").getValue().toString();
                        id_mahasiswa = selected.child("id_mahasiswa").getValue().toString();
                        status = selected.child("status").getValue().toString();
                        count=count+1;

                        //perulangan cari data mahasiswa
                        final Query selectMahasiswaById = dbRef.child("Mahasiswa").orderByChild("id").equalTo(id_mahasiswa);
                        selectMahasiswaById.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Log.d("Tags","ngambil data beasiswa");
                                if(dataSnapshot.exists()){
                                    Log.d("Tags","data beasiswa ada");
                                    for (DataSnapshot selected : dataSnapshot.getChildren()){
                                        Log.d("Tags","data beasiswa ke");
                                        String judul = selected.child("nama").getValue().toString();



                                        NotifArrayList.add(new NotifModel(id_mahasiswa,id_beasiswa, judul, status));
                                        Log.d("Tagtes",id_mahasiswa+id_beasiswa+judul+status);
//
                                        adapter.notifyDataSetChanged();
                                    }



                                }else{
                                    Log.d("Tags","data beasiswa(lloping kedua ga ada");
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }

                    adapter = new NotifAdminAdapter(NotifArrayList);
                    recyclerView.setAdapter(adapter);
                }else{

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NotifAdminFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotifAdminFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotifAdminFragment newInstance(String param1, String param2) {
        NotifAdminFragment fragment = new NotifAdminFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notif_admin, container, false);
    }
}
