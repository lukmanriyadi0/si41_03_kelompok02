package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.i_beca.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class AdminLihatNilaiActivity extends AppCompatActivity {
    private Uri filePath;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    private PDFView pdfView;

    ///dari intent
    String id_user;
    private Toolbar toolbar;

    public AdminLihatNilaiActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_lihat_nilai);

        Intent intent = getIntent();
        id_user = intent.getStringExtra("id_mahasiswa");

        //getting views from layout
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Transkrip Nilai");
        toolbar.setTitleTextAppearance(getApplicationContext(), R.style.ToolbarTitleTextStyle);

        pdfView =findViewById(R.id.pdfView);
        String path = "Mahasiswa/"+id_user+"/filetranskrip.pdf";
        final StorageReference fileReference = storageReference.child(path);
        fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'


                fileReference.getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        // Use the bytes to display the image
                        pdfView.fromBytes(bytes).load();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle any errors
                        Log.d("TAG",exception.getMessage());
                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags","file not found");
            }
        });
    }
    }
