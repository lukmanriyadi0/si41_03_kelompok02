package com.example.i_beca;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class NotifAdminAdapter extends RecyclerView.Adapter<NotifAdminAdapter.ViewHolder>  {
    private ArrayList<NotifModel> notif;

    public NotifAdminAdapter(ArrayList<NotifModel> notif) {
        this.notif = notif;
    }
    @NonNull
    @Override
    public NotifAdminAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_notif_admin, parent, false);
        return new NotifAdminAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotifAdminAdapter.ViewHolder holder, int position) {
        holder.bind(notif.get(position));
    }

    @Override
    public int getItemCount() {
        return notif.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvtitle, tvsubtitle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvtitle = itemView.findViewById(R.id.txtTitle);
            tvsubtitle = itemView.findViewById(R.id.txtBodyNotif);
            itemView.setOnClickListener(this);
        }

        public void bind(NotifModel notifModel) {
            tvtitle.setText(notifModel.getTitle());
            Log.d("Tags","Status :"+notifModel.getSubtitle());
            tvsubtitle.setText("Mahasiswa " + notifModel.getTitle()+" melakukan apply");
        }

        @Override
        public void onClick(View view) {
            NotifModel notifModel = notif.get(getAdapterPosition());

            Intent intent = new Intent(view.getContext(), DetailNotifikasiAdmin.class);
            intent.putExtra("id_user", notifModel.getId_user());
            intent.putExtra("Id", notifModel.getId());
            intent.putExtra("Status", notifModel.getSubtitle());
            view.getContext().startActivity(intent);

        }
    }
}