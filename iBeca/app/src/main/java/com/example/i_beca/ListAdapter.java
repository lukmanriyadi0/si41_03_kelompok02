package com.example.i_beca;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> implements Filterable {
    private List<Beasiswa> beasiswas;
    private List<Beasiswa> beasiswasFull;
    private Context context;
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    DatabaseReference dbRef;

    public ListAdapter(List<Beasiswa> listbeasiswa, Context context) {
        this.beasiswas = listbeasiswa;
        this.context = context;
        beasiswasFull = new ArrayList<>(listbeasiswa);
    }

    @NonNull
    @Override
    public ListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_beasiswa, parent, false);
        ListAdapter.MyViewHolder mViewHolder = new ListAdapter.MyViewHolder(mView);
        return mViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final ListAdapter.MyViewHolder holder, int position) {

        Beasiswa BeasiswaPosition = beasiswas.get(position);
        final String id_beasiswa = String.valueOf(BeasiswaPosition.getId_beasiswa());
        final String desc = String.valueOf(BeasiswaPosition.getDeskripsi_beasiswa());
        final String Title =  String.valueOf(BeasiswaPosition.getNama_beasiswa());
        final String institusi = BeasiswaPosition.instansi;
        holder.mTitle.setText(BeasiswaPosition.getNama_beasiswa());
        holder.minstnasi.setText(BeasiswaPosition.instansi);
        holder.mId = id_beasiswa;

        storageReference.child("Beasiswa/"+id_beasiswa+"/FotoBeasiswa.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                String downloadUrl = uri.toString();
                // Got the download URL for 'users/me/profile.png'
                Glide.with(context).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.mImageView);
                Log.d("Test",uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
            }
        });


        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Detail_beasiswa.class);
                intent.putExtra("id", id_beasiswa);
                intent.putExtra("nama", Title);
                intent.putExtra("instansi", institusi);
                intent.putExtra("desc", desc);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return beasiswas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;
        TextView mTitle;
        TextView minstnasi;
        String mId;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.BeasiswaImage);
            mTitle = itemView.findViewById(R.id.tv_title);
            minstnasi = itemView.findViewById(R.id.tv_instansi);
        }
    }

    @Override
    public Filter getFilter() {
        return beasiswasFilter;
    }

    private Filter beasiswasFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Beasiswa> filteredList = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(beasiswasFull);
            }
            else{
                String filterpattern = constraint.toString().toLowerCase().trim();

                for (Beasiswa beasiswa : beasiswasFull){
                    if (beasiswa.getNama_beasiswa().toLowerCase().contains(filterpattern)){
                        filteredList.add(beasiswa);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            beasiswas.clear();
            beasiswas.addAll((List)results.values);
            notifyDataSetChanged();

        }
    };
}

