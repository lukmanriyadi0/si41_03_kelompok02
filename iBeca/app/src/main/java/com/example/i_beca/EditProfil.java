package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

public class EditProfil extends AppCompatActivity {
    private EditText etname, etbirthdate, etemail, etpass, etrepass;
    private Button btnUpdate;
    private CircleImageView profile_image;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;
    private Query query;

    private Mahasiswa mahasiswa;
    private String keyID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        etname = findViewById(R.id.etname);
        etbirthdate = findViewById(R.id.etbirthdate);
        etemail = findViewById(R.id.etemail);
        etpass = findViewById(R.id.etpass);
        etrepass = findViewById(R.id.etrepass);

        btnUpdate = findViewById(R.id.btnUpdate);
        profile_image = findViewById(R.id.profile_image);

        Intent intent = getIntent();
        keyID = intent.getStringExtra("key_id");
        getProfile();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            firebaseUser = firebaseAuth.getCurrentUser();

            Log.d("ID", firebaseUser.getUid());
        }catch(Exception ex){
            Toast.makeText(this, "ID User tidak ditemukan, harap lakukan login ulang", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    public void doUpdate(View view) {
        String nama, tanggal, email;

        nama = etname.getText().toString();
        tanggal = etbirthdate.getText().toString();
        email = etemail.getText().toString();

        Map<String, Object> map = new HashMap<>();
        map.put("nama", nama);
        map.put("tanggal", tanggal);
        map.put("email", email);

        try {
            databaseReference.child("Mahasiswa").child(keyID).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(EditProfil.this, "Profile berhasil di update", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                    }
                }
            });
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private void getProfile(){
        query = databaseReference.child("Mahasiswa").orderByChild("id").equalTo(firebaseUser.getUid());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    for(DataSnapshot datas : dataSnapshot.getChildren()) {
                        keyID = datas.getKey();
                        etname.setText(datas.child("nama").getValue().toString());
                        etbirthdate.setText(datas.child("tanggal").getValue().toString());
                        etemail.setText(datas.child("email").getValue().toString());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
