package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ForgetPasswordVerificationActivity extends AppCompatActivity{
    Spinner spkeamanan;
    EditText etemail;
    EditText etjawaban;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_verification);

       spkeamanan = findViewById(R.id.keamanan);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.pertanyaan_keamanan, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spkeamanan.setAdapter(adapter);

        etemail = findViewById(R.id.et_email);
        etjawaban = findViewById(R.id.et_jawaban);
    }



    public void verifForget(View view) {
        final String keamanan,jawaban,email;
        email = etemail.getText().toString();
        keamanan = spkeamanan.getSelectedItem().toString();
        jawaban = etjawaban.getText().toString();

        final String[] emailDatabase = new String[1];
        final String[] passwordDatabase = new String[1];
        final String[] iduserDatabase = new String[1];
        final String[] namaDatabase = new String[1];
        final String[] tanggalDatabase = new String[1];
        final String[] keamananDatabase = new String[1];
        final String[] jawabanDatabase = new String[1];
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.show();

        DatabaseReference tableMahasiswarRef = database.getReference("Mahasiswa");

        Query checkEmail = tableMahasiswarRef.orderByChild("email").equalTo(email).limitToFirst(1);

        checkEmail.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean keamananSama=false;
                boolean jawabanSama=false;
                Log.d("Tags","Data ketemu");

                for (DataSnapshot user : dataSnapshot.getChildren()){

                    String idResult = user.child("id").getValue().toString();
                    String namaResult= user.child("nama").getValue().toString();
                    String tanggalResult = user.child("tanggal").getValue().toString();
                    String emailResult = user.child("email").getValue().toString();
                    String keamananResult = user.child("keamanan").getValue().toString();
                    String jawabanResult = user.child("jawaban").getValue().toString();
                    String passwordResult = user.child("password").getValue().toString();
                    Log.d("Tags","Data Dipindah");

                    if(keamanan.equals(keamananResult)){
                        keamananSama = true;
                        Log.d("Tags","Keamanan sama");
                        if(jawaban.equals(jawabanResult)){
                            jawabanSama = true;
                            iduserDatabase[0] = idResult;
                            namaDatabase[0] = namaResult;
                            tanggalDatabase[0] = tanggalResult;
                            emailDatabase[0] = emailResult;
                            passwordDatabase[0] = passwordResult;
                            keamananDatabase[0] = keamananResult;
                            jawabanDatabase[0] = jawabanResult;

                            Log.d("Tags","Jawaban sama");
//
                        }

                    }
                }

                if(keamananSama && jawabanSama == true){
                    progressDialog.dismiss();


                    Log.d("Tags","boleh reset password");
                    Bundle bundle = new Bundle();
                    bundle.putString("id", iduserDatabase[0]);
                    bundle.putString("nama", namaDatabase[0]);
                    bundle.putString("tanggal", tanggalDatabase[0]);
                    bundle.putString("email", emailDatabase[0]);
                    bundle.putString("password", passwordDatabase[0]);
                    bundle.putString("keamanan", keamananDatabase[0]);
                    bundle.putString("jawaban", jawabanDatabase[0]);

                    Toast.makeText(getApplicationContext(), "Verifikasi Berhasil", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), ForgetPasswordActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);

                }else{
                    progressDialog.dismiss();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });
    }
}
