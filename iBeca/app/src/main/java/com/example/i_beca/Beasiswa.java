package com.example.i_beca;

public class Beasiswa {
    String id_beasiswa;
    String nama_beasiswa;
    String instansi;
    String deskripsi_beasiswa;
    boolean Status;


    public Beasiswa() {
    }

    public Beasiswa(String id_beasiswa, String nama_beasiswa, String instansi, String deskripsi_beasiswa, boolean status) {
        this.id_beasiswa = id_beasiswa;
        this.nama_beasiswa = nama_beasiswa;
        this.instansi = instansi;
        this.deskripsi_beasiswa = deskripsi_beasiswa;
        Status = status;
    }

    public Beasiswa(Beasiswa value) {
        this.id_beasiswa = value.getId_beasiswa();
        this.nama_beasiswa = value.getNama_beasiswa();
        this.instansi = value.getInstansi();
        this.deskripsi_beasiswa = value.getDeskripsi_beasiswa();
        this.Status = value.isStatus();
    }

    public String getId_beasiswa() {
        return id_beasiswa;
    }

    public void setId_beasiswa(String id_beasiswa) {
        this.id_beasiswa = id_beasiswa;
    }

    public String getNama_beasiswa() {
        return nama_beasiswa;
    }

    public void setNama_beasiswa(String nama_beasiswa) {
        this.nama_beasiswa = nama_beasiswa;
    }

    public String getInstansi() {
        return instansi;
    }

    public void setInstansi(String instansi) {
        this.instansi = instansi;
    }

    public String getDeskripsi_beasiswa() {
        return deskripsi_beasiswa;
    }

    public void setDeskripsi_beasiswa(String deskripsi_beasiswa) {
        this.deskripsi_beasiswa = deskripsi_beasiswa;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }
}
