package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DaftarActivity extends AppCompatActivity {
    private FirebaseAuth authRef;
    private FirebaseStorage databaseRef;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    EditText etname,ettgl,etemail,etpassword,etrepassword,etjawaban;
    Spinner spkeamanan;
    Button butreg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        etname = findViewById(R.id.name);
        ettgl = findViewById(R.id.tgl);
        etemail = findViewById(R.id.email);
        etpassword = findViewById(R.id.password);
        etrepassword = findViewById(R.id.repassword);
        butreg = findViewById(R.id.buttonreg);
        etjawaban=findViewById(R.id.jawaban);
        spkeamanan = (Spinner) findViewById(R.id.keamanan);

// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.pertanyaan_keamanan, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spkeamanan.setAdapter(adapter);

        authRef = FirebaseAuth.getInstance();

        butreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerNewUser();
            }
        });
    }


    private void registerNewUser() {
        final String nama,email,password,repassword,tanggal,keamanan,jawaban;
        String tanggal1 = null;
        final String[] iduser = new String[1];
        nama = etname.getText().toString();



        email = etemail.getText().toString();
        password = etpassword.getText().toString();
        repassword = etrepassword.getText().toString();
        keamanan = spkeamanan.getSelectedItem().toString();
        jawaban = etjawaban.getText().toString();



        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy"); // Make sure user insert date into edittext in this format.

        Date dateObject;


        try{
            tanggal1 =(ettgl.getText().toString());

            dateObject = formatter.parse(tanggal1);

            tanggal1 = new SimpleDateFormat("dd-MM-yyyy").format(dateObject);

        }

        catch (java.text.ParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d("Tags", e.toString());
        }

        tanggal = tanggal1;

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please enter email...", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Please enter password!", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(repassword)){
            Toast.makeText(getApplicationContext(), "Please enter repassword!", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(tanggal)){
            Toast.makeText(getApplicationContext(), "Please enter tanggal!", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(jawaban)){
            Toast.makeText(getApplicationContext(), "Please enter jawaban!", Toast.LENGTH_LONG).show();
            return;
        }

        if(!password.equals(repassword)){
            Toast.makeText(getApplicationContext(), "password doesnt match!", Toast.LENGTH_LONG).show();
            return;
        }


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.show();

        authRef.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = task.getResult().getUser();
                            iduser[0] = user.getUid();
                            DatabaseReference tableRef = database.getReference("Mahasiswa");
                            Mahasiswa mhs = new Mahasiswa(iduser[0],nama,tanggal,email,password,keamanan,jawaban);


                            tableRef.push().setValue(mhs);
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Registration successful!", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(DaftarActivity.this, AppIntroActivity.class);
                            startActivity(intent);
                        }
                        else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Registration failed! Please try again later", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

}
