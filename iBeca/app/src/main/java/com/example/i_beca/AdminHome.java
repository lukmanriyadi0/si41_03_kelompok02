package com.example.i_beca;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AdminHome#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdminHome extends Fragment {

    private DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mahasiswaRef = databaseRef.child("Mahasiswa");
    DatabaseReference beasiswaRef = databaseRef.child("Beasiswa");
    DatabaseReference transaksiRef = databaseRef.child("Transaksi");

    TextView txtJumlahMahasiswa,txtJumlahBeasiswa,txtJumlahPendaftaan;
    public  int jumlahMahasiswa, jumlahBeasiswa, jumlahPendaftaran;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String useradmin;
    private FirebaseAuth firebaseAuth;
    SharedPref sharedPref;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //logout
        logout =view.findViewById(R.id.logout);
        firebaseAuth = FirebaseAuth.getInstance();
        sharedPref = new SharedPref(getContext());
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getContext(), AdminLihatNilaiActivity.class));

                sharedPref.clearCredential(firebaseAuth.getUid());
                firebaseAuth.signOut();
                Toast.makeText(getContext(), "Berhasil Logout", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getContext(), LoginsignupActivity.class);
                startActivity(intent);

            }
        });

        txtJumlahBeasiswa = view.findViewById(R.id.beasiswavalue);
        txtJumlahMahasiswa = view.findViewById(R.id.mahasiswavalue);
        txtJumlahPendaftaan = view.findViewById(R.id.pendaftarvalue);

        //get jumlah
        mahasiswaRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                jumlahMahasiswa = (int) dataSnapshot.getChildrenCount();
                Log.d("Tags","jumlah mahasiswa"+jumlahMahasiswa);
                txtJumlahMahasiswa.setText(""+jumlahMahasiswa);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        beasiswaRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                jumlahBeasiswa = (int) dataSnapshot.getChildrenCount();
                Log.d("Tags","jumlah beasiswa"+jumlahBeasiswa);
                txtJumlahBeasiswa.setText(""+jumlahBeasiswa);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        transaksiRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                jumlahPendaftaran = (int) dataSnapshot.getChildrenCount();
                Log.d("Tags","jumlah pendaftaran"+jumlahPendaftaran);
                txtJumlahPendaftaan.setText(""+jumlahPendaftaran);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private TextView logout;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AdminHome() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AdminHome.
     */
    // TODO: Rename and change types and number of parameters
    public static AdminHome newInstance(String param1, String param2) {
        AdminHome fragment = new AdminHome();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    //    @Override
//    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
//        txtJumlahBeasiswa = view.findViewById(R.id.beasiswavalue);
//        txtJumlahMahasiswa = view.findViewById(R.id.mahasiswavalue);
//        txtJumlahPendaftaan = view.findViewById(R.id.pendaftarvalue);
//
//
////        try {
////
////
////            Query countMahasiswa = mahasiswaRef.orderByChild("id");
////
////            countMahasiswa.addListenerForSingleValueEvent(new ValueEventListener() {
////                @Override
////                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
////                    try {
////                        int size = (int) dataSnapshot.getChildrenCount();
////                    } catch (Exception ex) {
////                        ex.printStackTrace();
////                    }
////                }
////
////                @Override
////                public void onCancelled(@NonNull DatabaseError databaseError) {
////
////                }
////            });
////        } catch (Exception ex) {
////            ex.printStackTrace();
////        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_home, container, false);






        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);


    }


}
