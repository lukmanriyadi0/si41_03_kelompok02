package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class DetailNotifActivity extends AppCompatActivity {
    private ImageView back;
    private String id_beasiswa,status,judul,instansi;
    TextView title,institusi,nama_mhs,desc,penutup;
    ImageView header;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    String downloadUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_notif);
        Intent intent = getIntent();
        id_beasiswa=intent.getStringExtra("Id");
        status=intent.getStringExtra("Status");

        title=findViewById(R.id.beasiswaTitle);
        institusi=findViewById(R.id.institusiTitle);
        nama_mhs=findViewById(R.id.notiftext1);
        desc=findViewById(R.id.notiftext2);
        penutup=findViewById(R.id.notiftext3);
        header=findViewById(R.id.beasiswaimage);


        nama_mhs.setText("Halo");





        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
        final Query selectBeasiswaById = dbRef.child("Beasiswa").orderByChild("id_beasiswa").equalTo(id_beasiswa);

        selectBeasiswaById.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    Beasiswa beasiswa = new Beasiswa();
                    for (DataSnapshot selected : dataSnapshot.getChildren()){
                        judul = selected.child("nama_beasiswa").getValue().toString();
                        instansi = selected.child("instansi").getValue().toString();

                        title.setText(judul);
                        institusi.setText(instansi);
                    }


                }else{

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //ganti isi notif
        Log.d("Tags",status);
        Log.d("Tags",id_beasiswa);
        if(status.equalsIgnoreCase("Terima")){
            desc.setText("Selamat kamu sudah lolos pada beasiswa ini");
            penutup.setText("Silahkan ke ruangan kemahasiswaan di kampus untuk detail penerimaan beasiswanya," +("\n")+"Terimakasih");
        }
        else {
            desc.setText("Mohon maaf kamu tidak lolos pada beasiswa ini");
            penutup.setText("Semangat terus jangan pantang menyerah, "+("\n")+"Terimakasih");
        }


        ///ganti gambar dari storage


        storageReference.child("Beasiswa/"+id_beasiswa+"/FotoBeasiswa.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                downloadUrl = uri.toString();
                // Got the download URL for 'users/me/profile.png'
                Glide.with(DetailNotifActivity.this).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(header);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        });

        Log.d("tags","berhasil ambil gambar");


        //back arrow
        back=(ImageView)findViewById(R.id.imageView4);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(),NotifActivity.class);
                startActivity(intent);

            }
        });


    }
}
