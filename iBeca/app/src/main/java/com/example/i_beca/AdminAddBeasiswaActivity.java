package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.UUID;

public class AdminAddBeasiswaActivity extends AppCompatActivity{
    //a constant to track the file chooser intent
    private static final int PICK_IMAGE_REQUEST = 134;

    ImageView imgView;

    EditText etnamabeasiswa,etinstansi,etdescbeasiswa;

    Button btnadd;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();

    //a Uri object to store file path
    private Uri filePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_beasiswa);


        etnamabeasiswa = findViewById(R.id.et_beasiswaname);
        etinstansi = findViewById(R.id.et_instansi);
        etdescbeasiswa = findViewById(R.id.et_deskripsi);

        btnadd = findViewById(R.id.btnAdd);

        imgView = findViewById(R.id.imgHeader);

        Log.d("tags","masuk activity");

    }



    //method to show file chooser
    private void showFileChooser() {
        Log.d("tags","pict img");
        //for greater than lolipop versions we need the permissions asked on runtime
        //so if the permission is not available user will go to the screen to allow storage permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }




    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("tags","pict img beerhasil");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imgView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void uploadBeasiswa(){
        String namaBeasiswa, instansi, descBeasiswa;
        String id_beasiswa = String.valueOf(UUID.randomUUID());
        namaBeasiswa = etnamabeasiswa.getText().toString();
        instansi = etinstansi.getText().toString();
        descBeasiswa = etdescbeasiswa.getText().toString();

        DatabaseReference beasiswaRef = dbRef.child("Beasiswa");

        StorageReference fotoRef = storageReference.child("Beasiswa/"+id_beasiswa+"/FotoBeasiswa.jpg");


        if(filePath != null){

            //insert ke db dulu
            Beasiswa b = new Beasiswa(id_beasiswa,namaBeasiswa,instansi,descBeasiswa,true);
            beasiswaRef.push().setValue(b);

            //displaying a progress dialog while upload is going on
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.show();

            fotoRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            progressDialog.dismiss();
                            //and displaying a success toast
                            Toast.makeText(getApplicationContext(), "Data Uploaded ", Toast.LENGTH_LONG).show();
                            reset();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //if the upload is not successfull
                    //hiding the progress dialog
                    progressDialog.dismiss();

                    //and displaying error message
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    //calculating progress percentage
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                    //displaying percentage in progress dialog
                    progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                }
            });
        }else{
            //and displaying error message
            Toast.makeText(getApplicationContext(), "Foto Masih Kosong", Toast.LENGTH_LONG).show();
            return;
        }

    }

    public void uploadFoto(View view) {
        showFileChooser();
    }

    public void clickUploaBeasiswa(View view) {
        uploadBeasiswa();
    }

    public void reset(){
        imgView.setImageResource(0);
        filePath = null;
        etdescbeasiswa.setText("");
        etinstansi.setText("");
        etnamabeasiswa.setText("");
    }
}

