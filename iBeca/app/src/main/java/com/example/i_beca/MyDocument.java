package com.example.i_beca;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MyDocument extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydocument);
    }

    public void pasFoto(View view) {
        Intent intent = new Intent(this, UploadPasFotoActivity.class);
        startActivity(intent);
    }

    public void transkripNilai(View view) {
        Intent intent = new Intent(this, UploadTranskripNilaiActivity.class);
        startActivity(intent);
    }

    public void motivationLetter(View view) {
        Intent intent = new Intent(this, UploadMotivationLetterActivity.class);
        startActivity(intent);
    }

    public void infoGaji(View view) {
        Intent intent = new Intent(this, UploadInfoGajiActivity.class);
        startActivity(intent);
    }

}
