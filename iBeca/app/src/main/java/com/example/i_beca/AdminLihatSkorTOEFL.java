package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class AdminLihatSkorTOEFL extends AppCompatActivity {

    private Uri filePath;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    private ImageView skorTOEFL;
    String id_user;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_lihat_skor_toefl);

        Intent intent = getIntent();
        id_user = intent.getStringExtra("id_user");


        //getting views from layout
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Skor TOEFL");
        toolbar.setTitleTextAppearance(getApplicationContext(), R.style.ToolbarTitleTextStyle);

        skorTOEFL =findViewById(R.id.skorToefl);

        storageReference.child("Mahasiswa/" + id_user + "/Fileskortoefl.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                Glide.with(AdminLihatSkorTOEFL.this).load(uri.toString())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(skorTOEFL);
                Log.d("Test", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
            }
        });

    }
}
