package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class DetailMahasiswaApply extends AppCompatActivity {

    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mahasiswaRef = databaseReference.child("Mahasiswa");
    DatabaseReference beasiswaRef = databaseReference.child("Beasiswa");
    DatabaseReference transaksiRef = databaseReference.child("Transaksi");
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    String id_mahasiswa,id_beasiswa,nama_mahasiswa;
    TextView txtnamaMahasiswa;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mahasiswa_apply);
        txtnamaMahasiswa = findViewById(R.id.tvName);

            Intent as = getIntent();
            id_mahasiswa = as.getStringExtra("id_mahasiswa");
            id_beasiswa = as.getStringExtra("id_beasiswa");
            nama_mahasiswa = as.getStringExtra("nama_mahasiswa");

        txtnamaMahasiswa.setText(nama_mahasiswa);
        Log.d("tags","id detail "+id_mahasiswa);
        imageView = findViewById(R.id.profPic);

        storageReference.child("Mahasiswa/"+id_mahasiswa+"/Filefoto.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                Glide.with(DetailMahasiswaApply.this).load(uri.toString())
                        .fitCenter() // menyesuaikan ukuran imageview
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
                Log.d("Test","ini uri foto :" +uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("tags",exception.getMessage());
            }
        });

    }

    public void fotoApplicant(View view) {
        Intent intent = new Intent(this, AdminLihatFoto.class);
        intent.putExtra("id_mahasiswa", id_mahasiswa);
        startActivity(intent);
    }

    public void transkripApplicant(View view) {
        Intent intent = new Intent(this, AdminLihatNilaiActivity.class);
        intent.putExtra("id_mahasiswa", id_mahasiswa);
        startActivity(intent);
    }

    public void motivApplicant(View view) {
        Intent intent = new Intent(this, AdminLihatMotiv.class);
        intent.putExtra("id_mahasiswa", id_mahasiswa);
        startActivity(intent);
    }

    public void cvApplicant(View view) {
        Intent intent = new Intent(this, AdminLihatCv.class);
        intent.putExtra("id_mahasiswa", id_mahasiswa);
        startActivity(intent);
    }

    public void skorApplicant(View view) {
        Intent intent = new Intent(this, AdminLihatSkor.class);
        intent.putExtra("id_mahasiswa", id_mahasiswa);
        startActivity(intent);
    }

    public void gajiApplicant(View view) {
        Intent intent = new Intent(this, AdminLihatGaji.class);
        intent.putExtra("id_mahasiswa", id_mahasiswa);
        startActivity(intent);
    }

    public void doApprove(View view) {

        final Query terima = transaksiRef.orderByChild("id_beasiswa").equalTo(id_beasiswa);


        terima.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.d("tags","data beasiswa ditemukan");
                    for(DataSnapshot beasiswa : dataSnapshot.getChildren()){
                        String id_mhs  = beasiswa.child("id_mahasiswa").getValue().toString();
                        Log.d("tags","data beasiswa ke");
                        if(id_mhs.equals(id_mahasiswa)){
                            Log.d("tags","data mahasiswa ditemukan");
                            Log.d("tags","idmahasiswa"+id_mahasiswa+ "  id_real :"+id_mhs);
////                                String id_mhs_real = id_mahasiswa;
//                                String id_beasiswa_real = id_beasiswa;
////                                String status  = "Terima";
                                ApplyBeasiswa doApprove = new ApplyBeasiswa(id_beasiswa,id_mahasiswa,"Terima");

//
                                beasiswa.getRef().setValue(doApprove);
//                                Log.d("tags","data trans diganti");
                            Toast.makeText(getApplicationContext(),"Berhasil Menerima ! ", Toast.LENGTH_LONG).show();

                        }

                    }
                }else{

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void doDecline(View view) {


        final Query tolak = transaksiRef.orderByChild("id_beasiswa").equalTo(id_beasiswa);


        tolak.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){


                    Log.d("tags","data beasiswa ditemukan");
                    for(DataSnapshot beasiswa : dataSnapshot.getChildren()){
                        Log.d("tags","data beasiswa ke");

                            String id_mhs  = beasiswa.child("id_mahasiswa").getValue().toString();

                            if(id_mhs.equals(id_mahasiswa)){
                                Log.d("tags","data mahasiswa ditemukan");
                                Log.d("tags","idmahasiswa"+id_mahasiswa+ "  id_real :"+id_mhs);
//                                String id_mhs_real = id_mahasiswa;
//                                String id_beasiswa_real = id_beasiswa;
//                                String status  = "Tolak";
                                ApplyBeasiswa doDecline = new ApplyBeasiswa(id_beasiswa,id_mahasiswa,"Tolak");
//
                                beasiswa.getRef().setValue(doDecline);
//                                Log.d("tags","data beasiswa diganti");
                                Toast.makeText(getApplicationContext(),"Berhasil Menolak ! ", Toast.LENGTH_LONG).show();
//                                finish();
                            }


                    }
                }else{

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    class ApplyBeasiswa{
        private String id_beasiswa, id_mahasiswa, status;

        public ApplyBeasiswa(){}

        public ApplyBeasiswa(String id_beasiswa, String id_mahasiswa, String status) {
            this.id_beasiswa = id_beasiswa;
            this.id_mahasiswa = id_mahasiswa;
            this.status = status;
        }

        public String getId_beasiswa() {
            return id_beasiswa;
        }

        public void setId_beasiswa(String id_beasiswa) {
            this.id_beasiswa = id_beasiswa;
        }

        public String getId_mahasiswa() {
            return id_mahasiswa;
        }

        public void setId_mahasiswa(String id_mahasiswa) {
            this.id_mahasiswa = id_mahasiswa;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

}

