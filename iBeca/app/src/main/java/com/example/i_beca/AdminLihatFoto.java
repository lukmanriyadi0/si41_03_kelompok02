package com.example.i_beca;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class AdminLihatFoto extends AppCompatActivity {

    private Uri filePath;

    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    private ImageView pasFoto;
    String id_user;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_lihat_foto);

        Intent intent = getIntent();
        id_user = intent.getStringExtra("id_mahasiswa");

        //getting views from layout
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Pas Foto");
        toolbar.setTitleTextAppearance(getApplicationContext(), R.style.ToolbarTitleTextStyle);

        pasFoto =findViewById(R.id.pasFoto);

        storageReference.child("Mahasiswa/" + id_user + "/Filefoto.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                Glide.with(AdminLihatFoto.this).load(uri.toString())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(pasFoto);
                Log.d("Test", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("Tags", exception.getMessage());
            }
        });

    }
}
