package com.example.i_beca;

public class HistoryModel {
    private int mImageResource;
    private String mTextDate,mTextName,mTextParticipant,mTextStatus,mTextTahap;


    public  HistoryModel (String textDate,int imageResource,String textName,String textParticipant,String textTahap,String textStatus){
        mImageResource = imageResource;
        mTextDate = textDate;
        mTextName = textName;
        mTextParticipant = textParticipant;
        mTextTahap = textTahap;
        mTextStatus = textStatus;
    }

    public int getImageResource() {
        return mImageResource;
    }

    public String getTextDate() {
        return mTextDate;
    }

    public String getTextName() {
        return mTextName;
    }

    public String getTextParticipant() {
        return mTextParticipant;
    }

    public String getTextTahap() {
        return mTextTahap;
    }

    public String getTextStatus() {
        return mTextStatus;
    }
}
