package com.example.i_beca;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AdminBeasiswaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdminBeasiswaFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private ListAdminAdapter adapter;
    private List<Beasiswa> beasiswaArrayList;
    private GridLayoutManager setLayoutManager;
    private ImageView add;
    private EditText search;
    DatabaseReference dbRef;

    public AdminBeasiswaFragment() {
        // Required empty public constructor
    }


    public static AdminBeasiswaFragment newInstance(String param1, String param2) {
        AdminBeasiswaFragment fragment = new AdminBeasiswaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        search=view.findViewById(R.id.search_field);

        //add
        add =view.findViewById(R.id.imageView4);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AdminAddBeasiswaActivity.class));

            }
        });








        //listbeasiswa
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Loading..");
        progressDialog.show();
        recyclerView = (RecyclerView)getView().findViewById(R.id.beasiswa);
        setLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(setLayoutManager);
        beasiswaArrayList = new ArrayList<>();

        dbRef = FirebaseDatabase.getInstance().getReference("Beasiswa");
        dbRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    Beasiswa beasiswa = postSnapshot.getValue(Beasiswa.class);
                    System.out.println(beasiswa);
                    beasiswaArrayList.add(new Beasiswa(beasiswa));

                }
                adapter = new ListAdminAdapter(beasiswaArrayList,getContext());
                recyclerView.setAdapter(adapter);


                progressDialog.dismiss();

                //search

                search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(search.getText()==null){

                        }
                        else{
                            adapter.getFilter().filter(search.getText());
                        }


                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), databaseError.getMessage(),Toast.LENGTH_SHORT).show();


            }
        });




    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_beasiswa, container, false);
    }

}
